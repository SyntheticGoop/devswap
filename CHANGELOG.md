# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/SyntheticGoop/devswap/compare/v1.1.0...v1.2.0) (2021-03-18)


### Features

* Allow empty exec ([1ba5789](https://gitlab.com/SyntheticGoop/devswap/commit/1ba5789fe04e661ae7f2b1c343749d402d0fc76b))

## [1.1.0](https://gitlab.com/SyntheticGoop/devswap/compare/v1.0.0...v1.1.0) (2021-03-16)


### Features

* Add partial swapping, add newline after JSON format. ([d721048](https://gitlab.com/SyntheticGoop/devswap/commit/d72104818b0b227682e4822f476a8309d31f3b37))

## 1.0.0 (2021-02-05)


### Features

* create devswap cli tool ([6b3db38](https://gitlab.com/SyntheticGoop/devswap/commit/6b3db3828ba3c9e1fb91e8a5a40449e9703139bb))
