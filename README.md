# devswap

This command line tool allows you to swap out the package.json `main` field with the `devswap` field during a command line action.

This package was created to allow typescript only workspaces during development, then swap out the main field from the typescript package to the built package during the release stage.

## Usage

Install the package into your repository.

```bash
yarn add devswap -D
npm install devswap -D
```

Setup the fields you wish to swap in your `package.json`.

```json
{
  "name": "app",
  "main": "src/main.ts",
  "devswap:main": "build/cjs/main.js",
  "devswap:module": "build/esm/main.js"
}
```

During the command, the fields will be replaced as such.

```json
{
  "name": "app",
  "main": "build/cjs/main.js",
  "module": "build/esm/main.js"
}
```

Note that at the end of the command, the **original json file** will be restored. If your command mutates the JSON file, the changes will be lost.

Then setup your devswap commands on your `package.json`.

```json
{
  "scripts": {
    "publish": "devswap -e 'yarn npm publish'",
    "pack": "devswap -e 'yarn pack'",
    "test": "devswap -e 'yarn test'"
  }
}
```

Now run your command.

```bash
yarn run test
yarn run pack
yarn run publish
```

You can use `devswap` partially. However, you must provide keys in pairs (ie. Each key prefixed with "devswap:" must have an existing accompanying key).

During the command, all "devswap:" prefixes will be removed. All swapped keys will be prefixed "swapdev:".

The flag to use is `-i`.

```bash
devswap -i -e 'echo true'
```

The reverse can be done with the `-o` flag.

```bash
devswap -o -e 'echo true'
```
