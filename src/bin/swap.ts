import { command } from 'execa'
import yargs from 'yargs'
import { swap } from '../main'

async function main() {
  const config = yargs(process.argv.slice(2)).options({
    exec: {
      alias: 'e',
      type: 'string',
      description: 'The command to execute (uses shelljs sh by default) that will be wrapped between jsmain swap calls.',
    },
    in: {
      alias: 'i',
      type: 'boolean',
      description: 'Swap in',
      conflicts: ['o', 'out']
    },
    out: {
      alias: 'o',
      type: 'boolean',
      description: 'Swap out',
      conflicts: ['i', 'in']
    }
  }).argv

  await swap(() => config.exec ? command(config.exec, { shell: true }) : {}, config.in ? 'swapin' : config.out ? 'swapout' : undefined)
}

main()