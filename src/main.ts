import death from 'death'
import { existsSync } from 'fs'
import { readFile, writeFile } from 'fs/promises'

function rotate(package_json: Record<string, any>, keyin: string, keyout: string) {
  let next: Record<string, any> = {}

  const swapped = new Set(
    Object.keys(package_json)
      .filter(x => x.startsWith(keyin))
      .map(x => x.slice(keyin.length))
  )

  for (const [key, val] of Object.entries(package_json)
    .map(([key, val]) => key.startsWith(keyin) ? [key.slice(keyin.length), val] : [swapped.has(key) ? keyout + key : key, val])
  ) {
    next[key] = val
  }

  return next
}



export async function swap(action: () => Promise<any> | any, partial?: 'swapin' | 'swapout') {
  if (!existsSync('package.json')) throw Error('This function must be called in a directory with a package.json!')

  const original = await readFile('package.json', { encoding: 'utf8' })

  const swapped = !partial || partial === 'swapin' ? rotate(JSON.parse(original), 'devswap:', 'swapdev:') : rotate(JSON.parse(original), 'swapdev:', 'devswap:')

  death(async sig => {
    await writeFile('package.json', original, 'utf8')
    process.exit(1)
  })


  await writeFile('package.json', JSON.stringify(swapped, null, 2) + '\n', 'utf8')

  try {
    await action()
  } catch (e) {
    console.log(e)
  } finally {
    if (partial) return
    await writeFile('package.json', original, 'utf8')
  }
}